//
//  APIManager.swift
//  TestPlaces
//
//  Created by Miguel Gomes on 26/06/17.
//  Copyright © 2017 Miguel Gomes. All rights reserved.
//

import Foundation

private let autocomplete_base_url = "https://maps.googleapis.com/maps/api/place/autocomplete"
private let autocomplete_api_key = "AIzaSyClTydjdbkvDJbhgzGictSEAXPPIEJuXZE"
private let place_detail_base_url = "https://maps.googleapis.com/maps/api/place/details"
private let details_api_key = "AIzaSyAYo5Ku8sIVCmlpyJdqoLDA733l5IejCOk"

private let kPredictions = "predictions"
private let kResult = "result"

class APIManager {
    static func getPlacesResults(searchText: String, successFunction: (([Place]) -> Void)) {
        let session = NSURLSession.sharedSession()
        if let url = NSURL(string:"\(autocomplete_base_url)/json?input=\(searchText)&key=\(autocomplete_api_key)") {
            let request = NSMutableURLRequest(URL: url)
            session.dataTaskWithRequest(request)
            
            let task = session.dataTaskWithRequest(request) {
                (data, response, error) -> Void in
                
                if let data = data {
                    do{
                        let jsonR = try NSJSONSerialization.JSONObjectWithData(data, options:.AllowFragments)
                        let listPredictions = jsonR[kPredictions] as! NSArray
                        let results = listPredictions.map{Place(dictionary:$0 as! NSDictionary)}
                        successFunction(results)
                    }
                    catch {
                        // TODO: Hanlde error
                        print("Error with Json: \(error)")
                    }
                }
            }
            task.resume()
        }
    }
    
    static func getPlaceDetails(placeId: String, successFunction: ((PlaceDetails) -> Void)) {
        let session = NSURLSession.sharedSession()
        if let url = NSURL(string:"\(place_detail_base_url)/json?placeid=\(placeId)&key=\(details_api_key)") {
            let request = NSMutableURLRequest(URL: url)
            session.dataTaskWithRequest(request)
            
            let task = session.dataTaskWithRequest(request) {
                (data, response, error) -> Void in
                
                if let data = data {
                    do{
                        let jsonR = try NSJSONSerialization.JSONObjectWithData(data, options:.AllowFragments)
                        guard let result = jsonR[kResult] as? NSDictionary else {
                            // TODO: Hanlde error
                            return
                        }
                        successFunction(PlaceDetails(dictionary: result))
                    }
                    catch {
                        // TODO: Hanlde error
                        print("Error with Json: \(error)")
                    }
                }
            }
            task.resume()
        }
    }
}
