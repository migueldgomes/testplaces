//
//  Place.swift
//  TestPlaces
//
//  Created by Miguel Gomes on 26/06/17.
//  Copyright © 2017 Miguel Gomes. All rights reserved.
//

import Foundation

// TODO: enum with JSON keys
private let kDescription = "description"
private let kPlaceId = "place_id"


struct Place {
    var description: String?
    var placeId: String?
    
    init(dictionary: NSDictionary) {
        description = dictionary[kDescription] as? String
        placeId = dictionary[kPlaceId] as? String
    }
}
