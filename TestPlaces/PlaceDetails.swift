//
//  PlaceDetails.swift
//  TestPlaces
//
//  Created by Miguel Gomes on 27/06/17.
//  Copyright © 2017 Miguel Gomes. All rights reserved.
//

import Foundation

private let kGeometry = "geometry"
private let kLocation = "location"
private let kLatitude = "lat"
private let kLongitude = "lng"
private let kAddress = "formatted_address"

struct PlaceDetails {
    var latitude: Double?
    var longitude: Double?
    var address: String?
    
    init(dictionary: NSDictionary) {
        guard let geometry = dictionary[kGeometry] as? NSDictionary,
        let location = geometry[kLocation] as? NSDictionary else {
            // TODO: Hanlde error
            return
        }
        longitude = location[kLongitude] as? Double
        latitude = location[kLatitude] as? Double
        address = dictionary[kAddress] as? String
    }
}