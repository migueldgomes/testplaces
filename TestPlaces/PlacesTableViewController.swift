//
//  PlacesTableViewController.swift
//  TestPlaces
//
//  Created by Miguel Gomes on 26/06/17.
//  Copyright © 2017 Miguel Gomes. All rights reserved.
//

import UIKit

private let placeCellIdentifier = "PlaceCell"
// TODO: Create string enum
private let kReadyToShow = "Ready to show you some places!"

class PlacesTableViewController: UITableViewController {
    
    let searchController = UISearchController(searchResultsController: nil)
    var foundPlaces = [Place]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setup the Search Controller
        searchController.searchResultsUpdater = self
        searchController.searchBar.delegate = self
        definesPresentationContext = true
        searchController.dimsBackgroundDuringPresentation = false
        
        tableView.tableHeaderView = searchController.searchBar
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        var numOfSections: Int = 0
        if foundPlaces.count > 0
        {
            tableView.separatorStyle = .SingleLine
            numOfSections            = 1
            tableView.backgroundView = nil
        }
        else
        {
            // TODO: Add some image or something more user friendly instead of this simple label
            let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            noDataLabel.text          = kReadyToShow
            noDataLabel.textColor     = UIColor.blackColor()
            noDataLabel.textAlignment = .Center
            tableView.backgroundView  = noDataLabel
            tableView.separatorStyle  = .None
        }
        return numOfSections
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return foundPlaces.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(placeCellIdentifier, forIndexPath: indexPath)

        // Configure the cell...
        cell.textLabel?.text = foundPlaces[indexPath.row].description

        return cell
    }
    
    // MARK: - Table view delegate
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let place = foundPlaces[indexPath.row]
        guard let placeId = place.placeId else { return }
        // TODO: Add loading spinner here or in the next view
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        APIManager.getPlaceDetails(placeId, successFunction: gotPlaceDetails)
    }
    
    func gotPlaceDetails(placeDetails: PlaceDetails) {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
        dispatch_async(dispatch_get_main_queue()) {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let placeDetailVc = storyboard.instantiateViewControllerWithIdentifier("PlaceDetailViewController") as! PlaceDetailViewController
            placeDetailVc.placeDetails = placeDetails
            self.navigationController?.pushViewController(placeDetailVc, animated: true)
        }
    }
}
// MARK: - UISearchBar Delegate
extension PlacesTableViewController: UISearchBarDelegate {
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        self.foundPlaces.removeAll()
        //dispatch_async(dispatch_get_main_queue()) {
            self.tableView.reloadData()
        //}
    }
}
// MARK: - UISearchResultsUpdating Delegate
extension PlacesTableViewController: UISearchResultsUpdating {
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        let searchBar = searchController.searchBar
        if let searchText = searchBar.text where searchText != "" {
            // Get results from the Google Places JSON API endpoint and turn on network indicator (App Store searching style)
            UIApplication.sharedApplication().networkActivityIndicatorVisible = true
            APIManager.getPlacesResults(searchText, successFunction: gotPlacesResults)
        }
    }
    
    func gotPlacesResults(places:[Place]) {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
        self.foundPlaces = places
        dispatch_async(dispatch_get_main_queue()) {
            self.tableView.reloadData()
        }
    }
}
