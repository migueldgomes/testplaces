//
//  PlaceDetailViewController.swift
//  TestPlaces
//
//  Created by Miguel Gomes on 26/06/17.
//  Copyright © 2017 Miguel Gomes. All rights reserved.
//

import UIKit
import MapKit

class PlaceDetailViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    var placeDetails: PlaceDetails?
    let regionRadius: CLLocationDistance = 10000
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let details = self.placeDetails, let latitude = details.latitude, let longitude = details.longitude, let address = details.address else {
            // TODO: Handle error
            return
        }
        let placeLocation = CLLocation(latitude: latitude, longitude: longitude)
        self.navigationItem.title = address
        centerMapOnLocation(placeLocation)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

// MARK: - Helpers
extension PlaceDetailViewController {
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
                                                                  regionRadius * 3.0, regionRadius * 3.0)
        mapView.setRegion(coordinateRegion, animated: true)
    }
}